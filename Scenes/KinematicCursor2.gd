extends KinematicBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var grade_increment = 45
var tiempoDisparo = 0
var projectile_direction = Vector2()

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func _physics_process(delta):
	if Input.is_action_pressed("p2_down") and rotation_degrees >= 0:
		#rotation -= (radian_increment * delta)
		rotation_degrees -= (grade_increment * delta)
		#velocity = Vector2(1, 0).rotated(rotation)
		print(rotation_degrees)
	if Input.is_action_pressed("p2_up") and rotation_degrees <=90:
		rotation_degrees += (grade_increment * delta)
		print(rotation_degrees)
	projectile_direction = transform.x
	#print("x: " + str(transform.x))
	 
func _process(delta):
	tiempoDisparo +=delta
	if (Input.is_action_just_pressed("p2_shoot") and tiempoDisparo > 0.35): #and shoot_time >=0
		tiempoDisparo = 0
		var puntodisparo = get_node("./punto_disparo")
		print("origin: " + str(transform.origin))
		var bullet = preload("res://Scenes/Palomita_dulce_1.tscn").instance()
		bullet.add_collision_exception_with(self)
		bullet.position = Vector2(puntodisparo.get_global_transform().get_origin())
		bullet.apply_impulse( Vector2(0,0) , projectile_direction * -275)
		get_parent().add_child(bullet)
		if position.y > 700:
			bullet.queue_free()